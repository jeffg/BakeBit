#!/usr/bin/env python
#
# BakeBit example for the basic functions of BakeBit 128x64 OLED (http://wiki.friendlyarm.com/wiki/index.php/BakeBit_-_OLED_128x64)
#
# The BakeBit connects the NanoPi NEO and BakeBit sensors.
# You can learn more about BakeBit here:  http://wiki.friendlyarm.com/BakeBit
#
# Have a question about this example?  Ask on the forums here:  http://www.friendlyarm.com/Forum/
#
'''
## License

The MIT License (MIT)

BakeBit: an open source platform for connecting BakeBit Sensors to the NanoPi NEO.
Copyright (C) 2016 FriendlyARM

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''
from __future__ import print_function


import asyncio
import functools

import bakebit_128_64_oled_py3 as oled
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import time
import sys
import subprocess
import threading
import signal
import os
import socket
from threading import Timer
from time import sleep

import sys

global width
width=128
global height
height=64

# Vertical offset for page_index 1 and 2, some NanoHat's screens are offset in the aluminum case
global voffset
voffset=0

global pageCount
pageCount=2
global pageIndex
pageIndex=0
global showPageIndicator
showPageIndicator=False

EVENT_K1=2
EVENT_K2=3
EVENT_K3=4

global statsInterval
statsInterval = 4
global clockInterval
clockInterval = 1
global refreshInterval
refreshInterval=clockInterval

global autohideTimeout
autohideTimeout=120.0

oled.init()  #initialze SEEED OLED display
oled.setNormalDisplay()      #Set display to normal mode (i.e non-inverse mode)
oled.setHorizontalMode()

global image
image = Image.new('1', (width, height))
global draw
draw = ImageDraw.Draw(image)
global fontb24
fontb24 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 24);
global font14 
font14 = ImageFont.truetype('DejaVuSansMono.ttf', 14);
global smartFont
smartFont = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 10);
global fontb14
fontb14 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 14);
global font11
font11 = ImageFont.truetype('DejaVuSansMono.ttf', 11);

global timerlist
timerlist = {}

def cancelPreviousTimers(timerName):
    global timerlist

    if timerName in timerlist:
        for handle in timerlist[timerName]:
            handle.cancel()
        timerlist[timerName] = []

def addTimerHandle(timerName, handle):
    global timerlist

    if not timerName in timerlist:
        timerlist[timerName] = []
    timerlist[timerName].append(handle)

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def draw_page():
#    global drawing
    global image
    global draw
    global oled
    global font
    global font14
    global smartFont
    global width
    global height
    global pageCount
    global pageIndex
    global showPageIndicator
    global width
    global height
    global refreshInterval

    # Draw a black filled box to clear the image.            

    if pageIndex!=6:
        draw.rectangle((0,0,width,height), outline=0, fill=0)

    # Draw current page indicator
    if showPageIndicator:
        dotWidth=4
        dotPadding=2
        dotX=width-dotWidth-1
        dotTop=(height-pageCount*dotWidth-(pageCount-1)*dotPadding)/2
        for i in range(pageCount):
            if i==pageIndex:
                draw.rectangle((dotX, dotTop, dotX+dotWidth, dotTop+dotWidth), outline=255, fill=255)
            else:
                draw.rectangle((dotX, dotTop, dotX+dotWidth, dotTop+dotWidth), outline=255, fill=0)
            dotTop=dotTop+dotWidth+dotPadding

    if pageIndex==0:
        refreshInterval = clockInterval
        text = time.strftime("%A")
        draw.text((2,2),text,font=font14,fill=255)
        text = time.strftime("%e %b %Y")
        draw.text((2,18),text,font=font14,fill=255)
        text = time.strftime("%X")
        draw.text((2,40),text,font=fontb24,fill=255)
    elif pageIndex==1:
        # Draw some shapes.
        # First define some constants to allow easy resizing of shapes.
        refreshInterval = statsInterval
        padding = 2
        top = padding
        bottom = height-padding
        # Move left to right keeping track of the current x position for drawing shapes.
        x = 2
        IPAddress = get_ip()
        cmd = "top -bn1 | grep load | awk '{printf \"CPU: %.2f\", $(NF-2)}'"
        CPULOAD = subprocess.check_output(cmd, shell = True ).decode('utf-8')

        try:
            cmd = "cat /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"
            result = subprocess.check_output(cmd, shell = True ).decode('utf-8')
#            print(result)
            time_in_state = result.splitlines()
            KHzs = 0
            samples = 0 
            for state in time_in_state:
                params = state.split(' ')
                KHzs += (int(params[0]) * int(params[1]))
                samples += int(params[1])

            MHzAvg = KHzs / samples / 1000
#            print(MHzAvg)
            CPUMHz = str(int(MHzAvg)) + ' MHz'
        except:
            CPUMhz = '? MHz'

        # Reset counters after, so next time the average will be from the last run
        # When display was off (autohide) for a while, the first screen displayed
        # will have the average from last time, useful to know the average CPU frequency after a long "idle" period

        cmd = 'echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/stats/reset'
        subprocess.check_output(cmd, shell = True )

        CPU = CPULOAD + '  ' + CPUMHz
        cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
        MemUsage = subprocess.check_output(cmd, shell = True )
        cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%dGB %s\", $3,$2,$5}'"
        Disk = subprocess.check_output(cmd, shell = True )
        tempI = int(open('/sys/class/thermal/thermal_zone0/temp').read());
        if tempI>1000:
            tempI = tempI/1000
        tempStr = "SoC TEMP: %sC" % str(tempI)

        draw.text((x, top+voffset),       "IP: " + str(IPAddress),  font=smartFont, fill=255)
        draw.text((x, top+voffset+12),    CPU, font=smartFont, fill=255)
        draw.text((x, top+voffset+24),    MemUsage.decode("utf-8"),  font=smartFont, fill=255)
        draw.text((x, top+voffset+36),    Disk.decode("utf-8"),  font=smartFont, fill=255)
        draw.text((x, top+voffset+48),    tempStr,  font=smartFont, fill=255)
    elif pageIndex==2:
        # Draw some shapes.
        # First define some constants to allow easy resizing of shapes.
        refreshInterval = statsInterval
        padding = 2
        top = padding
        bottom = height-padding
        # Move left to right keeping track of the current x position for drawing shapes.
        x = 2
        cmd = "uptime | sed -E 's/^[^,]*up *//; s/, *[[:digit:]]* user.*//; s/min/m/; s/([[:digit:]]+) days, ?([[:digit:]]+):0?([[:digit:]]+)/\\1d \\2h \\3m/; s/([[:digit:]]+):0?([[:digit:]]+)/\\1h \\2m/'"
        UPTIME = subprocess.check_output(cmd, shell = True )
        cmd = "cat /sys/class/net/*/statistics/rx_bytes"
        lines = subprocess.check_output(cmd, shell = True ).decode('utf-8').splitlines()
        total = 0
        for line in lines:
            try:
                total+=int(line)
            except:
                pass

        RX = 'RX: ' + str(total)
        cmd = "cat /sys/class/net/*/statistics/tx_bytes"
        lines = subprocess.check_output(cmd, shell = True ).decode('utf-8').splitlines()
        total = 0
        for line in lines:
            try:
                total+=int(line)
            except:
                pass
        TX = 'TX: ' + str(total)

        cmd = "cat /sys/class/net/*/statistics/rx_packets"
        lines = subprocess.check_output(cmd, shell = True ).decode('utf-8').splitlines()
        total = 0
        for line in lines:
            try:
                total+=int(line)
            except:
                pass
        RXP = 'RX PKT: ' +str(total)
        cmd = "cat /sys/class/net/*/statistics/tx_packets"
        lines = subprocess.check_output(cmd, shell = True ).decode('utf-8').splitlines()
        total = 0
        for line in lines:
            try:
                total+=int(line)
            except:
                pass
        TXP = 'TX PKT: ' +str(total)

        draw.text((x, top+voffset),       'UP: ' + UPTIME.decode("utf-8"),  font=smartFont, fill=255)
        draw.text((x, top+voffset+12),    RX, font=smartFont, fill=255)
        draw.text((x, top+voffset+24),    TX,  font=smartFont, fill=255)
        draw.text((x, top+voffset+36),    RXP,  font=smartFont, fill=255)
        draw.text((x, top+voffset+48),    TXP,  font=smartFont, fill=255)
    elif pageIndex==3: #shutdown -- no
        draw.text((2, 2),  'Shutdown?',  font=font11, fill=255)

        draw.rectangle((2,16,width-4,16+13), outline=0, fill=0)
        draw.text((4, 18),  'Yes',  font=smartFont, fill=255)

        draw.rectangle((2,30,width-4,30+13), outline=0, fill=0)
        draw.text((4, 32),  'Reboot',  font=smartFont, fill=255)

        draw.rectangle((2,44,width-4,44+13), outline=0, fill=255)
        draw.text((4, 46),  'No',  font=smartFont, fill=0)

    elif pageIndex==3.5: #shutdown -- no
        draw.text((2, 2),  'Shutdown?',  font=font11, fill=255)

        draw.rectangle((2,16,width-4,16+13), outline=0, fill=0)
        draw.text((4, 18),  'Yes',  font=smartFont, fill=255)

        draw.rectangle((2,30,width-4,30+13), outline=0, fill=255)
        draw.text((4, 32),  'Reboot',  font=smartFont, fill=0)

        draw.rectangle((2,44,width-4,44+13), outline=0, fill=0)
        draw.text((4, 46),  'No',  font=smartFont, fill=255)

    elif pageIndex==4: #shutdown -- yes
        draw.text((2, 2),  'Shutdown?',  font=font11, fill=255)

        draw.rectangle((2,16,width-4,16+13), outline=0, fill=255)
        draw.text((4, 18),  'Yes',  font=smartFont, fill=0)

        draw.rectangle((2,30,width-4,30+13), outline=0, fill=0)
        draw.text((4, 32),  'Reboot',  font=smartFont, fill=255)

        draw.rectangle((2,44,width-4,44+13), outline=0, fill=0)
        draw.text((4, 46),  'No',  font=smartFont, fill=255)


    elif pageIndex==5:
        draw.text((2, 2),  'Shutting down',  font=fontb14, fill=255)
        draw.text((2, 20),  'Please wait',  font=font11, fill=255)

    elif pageIndex==5.5:
        draw.text((2, 2),  'Rebooting',  font=fontb14, fill=255)
        draw.text((2, 20),  'Please wait',  font=font11, fill=255)

    elif pageIndex==6:
        oled.clearDisplay()

    if pageIndex!=6:
        oled.drawImage(image)

def is_showing_power_msgbox():
    global pageIndex
    if pageIndex>=3 and pageIndex<=4:
        return True
    return False

def update_page_index(pi):
    global pageIndex
    pageIndex = pi

def onK(key, loop):
    global pageIndex

    cancelPreviousTimers('AUTOHIDE')
    addTimerHandle('AUTOHIDE', loop.call_later(autohideTimeout, onAutohide, loop))

    if pageIndex>=5 and pageIndex < 6:
        return

    if key == EVENT_K1:
        if is_showing_power_msgbox():
            if pageIndex==3:
                update_page_index(3.5)
            elif pageIndex==3.5:
                update_page_index(4)
            else:
                update_page_index(3)
            draw_page()
        else:
            update_page_index(0)
            draw_page()
    elif key == EVENT_K2:
        if is_showing_power_msgbox():
            if pageIndex==4:
                update_page_index(5)
                draw_page()
                time.sleep(2)
                oled.clearDisplay()
                os.system('systemctl poweroff')
                loop.stop()
                return
            elif pageIndex==3.5:
                update_page_index(5.5)
                draw_page()
                time.sleep(2)
                oled.clearDisplay()
                os.system('systemctl reboot')
                loop.stop()
                return
            else:
                update_page_index(0)
                draw_page()
        else:
            if pageIndex == 1:
                update_page_index(2)
            else:
                update_page_index(1)
            draw_page()
    elif key == EVENT_K3:
        if is_showing_power_msgbox():
            update_page_index(0)
            draw_page()
        else:
            update_page_index(3)
            draw_page()

    if pageIndex <= 2:
        cancelPreviousTimers('REFRESH')
        addTimerHandle('REFRESH', loop.call_at(int(loop.time())+refreshInterval, onRefresh, loop))

def receive_signal(sig, loop): #, stack):
    if sig == 'SIGUSR1':
        onK(EVENT_K1, loop)

    if sig == 'SIGUSR2':
        onK(EVENT_K2, loop)

    if sig == 'SIGALRM':
        onK(EVENT_K3, loop)

def onAutohide(loop):
    update_page_index(6)
    oled.clearDisplay()

def onRefresh(loop):

    if pageIndex >= 3:
        return

    draw_page()

    cancelPreviousTimers('REFRESH')
    addTimerHandle('REFRESH', loop.call_at(int(loop.time())+refreshInterval, onRefresh, loop))

def main():
    print ('Start')

    image0 = Image.open('friendllyelec.png').convert('1')
    oled.drawImage(image0)
    time.sleep(2)

    loop = asyncio.get_event_loop()
    for signame in ('SIGUSR1', 'SIGUSR2', 'SIGALRM'):
        loop.add_signal_handler(getattr(signal, signame),
                                functools.partial(receive_signal, signame, loop))
    loop.call_soon(onRefresh, loop)
    addTimerHandle('AUTOHIDE', loop.call_later(autohideTimeout, onAutohide, loop))

    try:
        loop.run_forever()
    finally:
        loop.close()

main()